<?php

use Faker\Generator as Faker;
use App\Repositories\Models\Blog;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'blog_name' => $faker->name,
    ];
});
