<?php

use Illuminate\Database\Seeder;
use App\Repositories\Models\Blog;

class BlogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Blog::class)->create();
    }
}
