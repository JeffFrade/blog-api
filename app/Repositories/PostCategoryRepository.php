<?php

namespace App\Repositories;

use App\Repositories\Models\PostCategory;

class PostCategoryRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new PostCategory();
    }
}
