<?php

namespace App\Repositories;

use App\Repositories\Models\Post;

class PostRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Post();
    }
}
