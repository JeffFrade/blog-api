<?php

namespace App\Repositories;

use App\Repositories\Models\Config;

class ConfigRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Config();
    }
}
