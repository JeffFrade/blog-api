<?php

namespace App\Repositories;

use App\Repositories\Models\Blog;

class BlogRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Blog();
    }
}
