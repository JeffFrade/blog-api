<?php

namespace App\Repositories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $table = 'comments';
    protected $primaryKey = 'comment_id';
    protected $fillable = [
        'comment', 'enrolled_id', 'post_id', 'enrolled_id'
    ];

    public function enrolled()
    {
        return $this->hasMany(Enrolled::class, 'enrolled_id', 'enrolled_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id', 'post_id');
    }
}
