<?php

namespace App\Repositories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';
    protected $primaryKey = 'category_id';
    protected $fillable = [
        'category'
    ];

    public function post()
    {
        return $this->hasMany(PostCategory::class, 'category_id', 'category_id');
    }
}
