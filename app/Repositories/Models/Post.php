<?php

namespace App\Repositories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $table = 'posts';
    protected $primaryKey = 'post_id';
    protected $fillable = [
        'post_name', 'post', 'comments', 'user_id', 'blog_id'
    ];

    public function media()
    {
        return $this->hasMany(Media::class, 'post_id', 'post_id');
    }

    public function  blog()
    {
        return $this->belongsTo(Blog::class, 'blog_id', 'blog_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'user_id', 'user_id');
    }

    public function category()
    {
        return $this->hasMany(PostCategory::class, 'post_id', 'post_id');
    }

    public function comment()
    {
        return $this->hasMany(Comment::class, 'post_id', 'post_id');
    }
}
