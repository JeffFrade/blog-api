<?php

namespace App\Repositories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Config extends Model
{
    use SoftDeletes;

    protected $table = 'configs';
    protected $primaryKey = 'config_id';
    protected $fillable = [
        'config_name', 'config_value', 'config_type', 'blog_id'
    ];

    public function blog()
    {
        return $this->hasMany(Blog::class, 'blog_id', 'blog_id');
    }
}
