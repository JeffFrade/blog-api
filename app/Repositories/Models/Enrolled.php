<?php

namespace App\Repositories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enrolled extends Model
{
    use SoftDeletes;

    protected $table = 'enrolleds';
    protected $primaryKey = 'enrolled_id';
    protected $fillable = [
        'name', 'email', 'password', 'send_emails', 'photo', 'blog_id'
    ];

    protected $hidden = [
        'password'
    ];

    public function comment()
    {
        return $this->hasOne(Comment::class, 'enrolled_id', 'enrolled_id');
    }

    public function blog()
    {
        return $this->belongsTo(Blog::class, 'blog_id', 'blog_id');
    }
}
