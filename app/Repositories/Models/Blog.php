<?php

namespace App\Repositories\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;

    protected $table = 'blogs';
    protected $primaryKey = 'blog_id';
    protected $fillable = [
        'blog_name'
    ];

    public function config()
    {
        return $this->belongsTo(Config::class, 'blog_id', 'blog_id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'blog_id', 'blog_id');
    }

    public function user()
    {
        return $this->hasMany(User::class, 'blog_id', 'blog_id');
    }

    public function enrolled()
    {
        return $this->hasMany(Enrolled::class, 'blog_id', 'blog_id');
    }
}
