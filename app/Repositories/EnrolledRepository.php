<?php

namespace App\Repositories;

use App\Repositories\Models\Enrolled;

class EnrolledRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Enrolled();
    }
}
