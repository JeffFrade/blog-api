<?php

namespace App\Repositories;

use App\Repositories\Models\Comment;

class CommentRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Comment();
    }
}
