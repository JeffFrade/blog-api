<?php

namespace App\Repositories;

use App\Repositories\Models\Media;

class MediaRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Media();
    }
}
