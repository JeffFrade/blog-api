<?php

namespace App\Repositories;

use App\Repositories\Models\Signature;

class SignatureRepository extends AbstractRepository
{
    public function __construct()
    {
        $this->model = new Signature();
    }
}
